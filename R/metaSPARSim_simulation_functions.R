### Function to concatenate (by column) a list of matrices
# Rows that are specific to only a matrix (or a subset of matrices) in the list, i.e. rows that are not present in all the matrices,
# are first added to the matrices that do not contain them, setting all the entry to zero. Rows are identified by the rownames()
# param:
#   - list of matrices to concatenate. For each matrix, the row and columns should be named.
# return: matrix representing the concatenation (by column) of the matrices contained in the input list
concatenate_matrix <- function(matrix_list) {
  # row ID
  row_id <- rownames(matrix_list[[1]])
  col_id <- colnames(matrix_list[[1]])
  N <- length(matrix_list)
  if (N > 1) {
    for (i in 2:N) {
      row_id <- union(row_id, rownames(matrix_list[[i]]))
      col_id <- c(col_id, colnames(matrix_list[[i]]))
    }
  }
  N_row <- length(row_id)
  N_col <- length(col_id)

  # create the final matrix
  final_matrix <- matrix(0, nrow = N_row, ncol = N_col)
  rownames(final_matrix) <- row_id
  colnames(final_matrix) <- col_id

  tmp_row_id <- rownames(matrix_list[[1]])
  tmp_col_id <- colnames(matrix_list[[1]])
  final_matrix[tmp_row_id, tmp_col_id] <-
    matrix_list[[1]][tmp_row_id, tmp_col_id]
  if (N > 1) {
    for (i in 2:N) {
      tmp_row_id <- rownames(matrix_list[[i]])
      tmp_col_id <- colnames(matrix_list[[i]])
      final_matrix[tmp_row_id, tmp_col_id] <-
        matrix_list[[i]][tmp_row_id, tmp_col_id]
    }
  }

  return(final_matrix)

}




# Function to simulate the biological variability (i.e. create biological replicates for a samples group)
# param:
#   - avgAbund: array containing the intensity values for each feature. It describes the intensity of a samples group
#   - phi: array containing the dispersion values for each feature. It describes the dispersion of a samples group
#   - nrep: number of biological replicates
#   - frag_scale: If not NULL, the sum by column of the output matrix are equal to the values in frag_scale. (Default: NULL)
# return: matrix (feature on rows, samples on columns) of gamma expression values
simulate_biovar <- function(avgAbund, phi, nrep, frag_scale = NULL) {
  if (is.null(frag_scale)) {
    scaling_factor <- rep(1, nrep)
  } else{
    scaling_factor <- frag_scale / sum(avgAbund)
  }

  # Number of species
  nspecies <- length(avgAbund)

  # Simulate biological variability
  gamma.mean <- avgAbund
  phi[is.na(phi)] <- 0
  phi[phi < 0] <- min(phi[phi > 0])

  gamma.shape <- 1 / phi
  gamma.scale <- phi * gamma.mean
  SimAbundances <- matrix(0, nrow = nspecies, ncol = nrep)
  for (j in 1:nspecies)
  {
    if (phi[j] > 0) {
      SimAbundances[j, ] <- rgamma(nrep,
                                   shape = gamma.shape[j],
                                   scale = gamma.scale[j] * scaling_factor)
    }
    else{
      SimAbundances[j, ] <- gamma.mean[j] * scaling_factor
    }
  }

  # No negative abundances: check
  SimAbundances[SimAbundances < 0] <- 0
  # Abundances are, by definition, integer numbers (but they are rescaled to integer in a following step: the gamma rescale step)
  rownames(SimAbundances) <- names(avgAbund)

  return(SimAbundances)
}




# Function to scale a give input matrix such that the sum by column is equal to the input parameter "new_libsize"
# Scaling is performed as following: scaled_matrix <- round(t(t(input_matrix)*new_libsize/colSums(input_matrix)))
# If after the rounding procedure the sum by column is different from the required ones, the most abundant entries of each column
# are modified (adding or removing 1) to obtain the given sum by column. Please note that such differences are usually very small
# and that modifying only the most abundant entries the impact on the overall distribution is minimal.
# Param:
#   - abund_matrix: matrix having features on rows and samples on columns
#   - new_libsize: integer array (having lenght equals to ncol(abund_matrix)) or scalar representing required sum by column after the scaling procedure
# return: a matrix (having the same dimensione of "abund_matrix") which a is a scaled version of the input matrix (such that the sum by columns are equals to the required ones)
simulate_rescaling <- function(abund_matrix, new_libsize) {
  # scale the matrix such that each column sum up to new_libsize
  scaled_abund_matrix <-
    round(t(t(abund_matrix) * new_libsize / colSums(abund_matrix)))

  N_rep <- ncol(abund_matrix)

  if (length(new_libsize) == 1) {
    # from scalar to array
    new_libsize <- rep(new_libsize, N_rep)
  }

  # since the columns could not sum exactly to the required values, modify the most abundant entries to adjust the sum
  for (rep in 1:N_rep) {
    diff_lib <- colSums(scaled_abund_matrix)[rep] - new_libsize[rep]

    if (diff_lib < 0) {
      ind_mod <-
        (order(scaled_abund_matrix[, rep], decreasing = TRUE)[1:abs(diff_lib)])

      scaled_abund_matrix[ind_mod, rep] <-
        scaled_abund_matrix[ind_mod, rep] + 1

    }
    if (diff_lib > 0) {
      ind_mod <-
        (order(scaled_abund_matrix[, rep], decreasing = TRUE)[1:abs(diff_lib)])

      scaled_abund_matrix[ind_mod, rep] <-
        scaled_abund_matrix[ind_mod, rep] - 1

    }
  }

  return (scaled_abund_matrix)
}



# Function to simulate the sequencing process (i.e. a multivariate hypergeometric on a gamma expression value array)
# param:
#   - avgAbund: array containing the intensity values for each feature. It describes the intensity of a single sample
#   - seqdepth: sequencing depth (i.e. sample size of the MH)
#   - digits: number of digits for random number generations
# return: array of length(avgAbund) elements representing the count values for the current sample
simulate_hyper <- function(avgAbund, seqdepth, digits) {
  if (seqdepth > 10 ^ digits) {
    print("seqdepth must be <= than 10^digits")
    return(NA)
  }

  ind <- which(avgAbund == 0)
  if (length(ind) > 0) {
    avgAbund_mod <- avgAbund[-ind]
  } else{
    avgAbund_mod <- avgAbund
  }
  cumulative <- cumsum(avgAbund_mod)

  index <- random_unif(seqdepth, digits)

  while (length(unique(index)) < seqdepth) {
    index <- unique(index)
    new_ind <- random_unif(seqdepth - length(index), digits)
    index <- c(index, new_ind)
  }

  check <- findInterval(index, cumulative, left.open = T) + 1
  count <- rep(0, length(avgAbund))
  names(count) <- names(avgAbund)
  to_change_red <- as.numeric(names(table(check)))
  to_change <- names(avgAbund_mod)[to_change_red]
  count[to_change] <- as.vector(table(check))

  return(count)
}
