// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>
#include <random>
#include <iostream>
#include <math.h>

using namespace Rcpp;

// [[Rcpp::export]]
NumericVector random_unif(int size, int digits){

  // vector to store the generated random number
  NumericVector index(size);

  // initialize the random generator
  std::random_device seed;

  std::default_random_engine rng(time(0));

  std::uniform_int_distribution<std::mt19937::result_type> unif09(0,9); // distribution in range [0, 9]

  // to obtaing number from 1 to 10^(digits), we generate number from 0 to 10^(digits)-1, so number having at most (digits-1) digits

  // vector containg the power of 10 from 10^0 to 10^(digits)
  NumericVector pow_val(digits);
  for (int j = 0; j < digits; ++j){
    pow_val[j] = pow(10,j);
  }

  // matrix containing the precomputed values of each digits [0,9] for each power of 10
  // rows are digits; columns are power of 10
  NumericMatrix digit_pow10(10, digits);

  // first row is all zeros
  for(int i_pow = 0; i_pow < digits; ++i_pow){
    digit_pow10(0, i_pow) = 0;
  }

  // initialize the matrix
  for(int i_digit = 1; i_digit < 10; ++i_digit){ //for digit 1..9
    for(int i_pow = 0; i_pow < digits; ++i_pow){ //from 10^0 to 10^(digits-1)
      digit_pow10(i_digit, i_pow) = i_digit * pow_val[i_pow];
    }
  }

  std::mt19937::result_type new_digit = 0;

  // for each number to generate
  for (int i = 0; i < size; ++i){
    index[i] = unif09(rng);

    for (int j = 1; j < digits; ++j){
      new_digit = unif09(rng);
      index[i] += digit_pow10(new_digit, j);
    }
    ++index[i];
  }

  return index;
}
