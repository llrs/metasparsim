\name{metaSPARSim}
\alias{metaSPARSim}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{16S rRNA gene sequencing count table simulation.
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
metaSPARSim(dataset_parameters)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{dataset_parameters}{
List containing features intensity, features variability and library sizes of each samples group. It can be obtained as estimate from real data with the "estimate_parameter_from_data" function or could be created by the users.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
data(HMP)
# Simulate a count table starting from real data estimates:
params<-estimate_parameter_from_data(data, data_norm, indpop,perc_not_zeros=.2)
names(params)<-names(indpop)
sim_data<-metaSPARSim(params)
str(sim_data$counts)
str(sim_data$gamma)
str(sim_data$params)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
